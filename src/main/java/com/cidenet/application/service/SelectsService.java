/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.service;

import com.cidenet.application.entities.AreaEmpleado;
import com.cidenet.application.entities.Pais;
import com.cidenet.application.entities.TipoIdentificacion;
import com.cidenet.application.repository.AreaEmpleadoRepository;
import com.cidenet.application.repository.PaisRepository;
import com.cidenet.application.repository.TipoIdentificacionRepository;
import com.cidenet.application.response.ResponseApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ruge2
 */
@Service
public class SelectsService {
    @Autowired
    private TipoIdentificacionRepository tipoIdentificacionRepository;
    
    @Autowired
    private AreaEmpleadoRepository areaEmpleadoRepository;
    
    @Autowired
    private PaisRepository paisRepository;
    
    public ResponseApi<TipoIdentificacion> listaTipoIdentificacion(){
        ResponseApi<TipoIdentificacion> responseApi = new ResponseApi<>();
        try{
            responseApi.setLista(tipoIdentificacionRepository.findAll());
            responseApi.setbRta(true);
            responseApi.setsMsg("Tipos de identificación encontrados");
            return responseApi;
        }
        catch(Exception e){
            System.out.println(e);
            responseApi.setbRta(false);
            responseApi.setsMsg("Ocurrio un error al cargar los tipos de identificación");
            return responseApi;
        }
    }
    
    public ResponseApi<AreaEmpleado> listaAreaEmpleado(){
        ResponseApi<AreaEmpleado> responseApi = new ResponseApi<>();
        try{
            responseApi.setLista(areaEmpleadoRepository.findAll());
            responseApi.setbRta(true);
            responseApi.setsMsg("Áreas encontradas");
            return responseApi;
        }
        catch(Exception e){
            System.out.println(e);
            responseApi.setbRta(false);
            responseApi.setsMsg("Ocurrio un error al cargar las áreas");
            return responseApi;
        }
    }
    
    public ResponseApi<Pais> listaPaisEmpleo(){
        ResponseApi<Pais> responseApi = new ResponseApi<>();
        try{
            responseApi.setLista(paisRepository.findAll());
            responseApi.setbRta(true);
            responseApi.setsMsg("Países encontradas");
            return responseApi;
        }
        catch(Exception e){
            System.out.println(e);
            responseApi.setbRta(false);
            responseApi.setsMsg("Ocurrio un error al cargar los paises");
            return responseApi;
        }
    }
}
