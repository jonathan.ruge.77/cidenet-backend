/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.service;

import com.cidenet.application.entities.AreaEmpleado;
import com.cidenet.application.entities.Empleado;
import com.cidenet.application.entities.Pais;
import com.cidenet.application.entities.TipoIdentificacion;
import com.cidenet.application.repository.AreaEmpleadoRepository;
import com.cidenet.application.repository.EmpleadoRepository;
import com.cidenet.application.repository.EmpleadoRepositoryCustom;
import com.cidenet.application.repository.PaisRepository;
import com.cidenet.application.repository.TipoIdentificacionRepository;
import com.cidenet.application.request.CrearEmpleadoRequest;
import com.cidenet.application.response.ResponseApi;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author ruge2
 */
@Service
public class EmpleadoService {
    
    @Autowired
    private EmpleadoRepository empleadoRepository;
    
    @Autowired
    private EmpleadoRepositoryCustom empleadoRepositoryCustom;
    
    @Autowired
    private TipoIdentificacionRepository tipoIdentificacionRepository;
    
    @Autowired
    private AreaEmpleadoRepository areaEmpleadoRepository;
    
    @Autowired
    private PaisRepository paisRepository;
    
    public ResponseApi<Empleado> crearEmpleado(CrearEmpleadoRequest crearEmpleadoRequest) {
        ResponseApi responseApi = new ResponseApi();
        try {
            responseApi = validarCampos(crearEmpleadoRequest);
            if (!responseApi.isbRta()) {
                return responseApi;
            }

            
            Optional<Pais> paisBuscado = paisRepository.findById(crearEmpleadoRequest.getPaisEmpleo());
            Optional<TipoIdentificacion> tipoIdentBuscado = tipoIdentificacionRepository.findById(crearEmpleadoRequest.getTipoIdentificacion());
            Optional<AreaEmpleado> areaBuscada = areaEmpleadoRepository.findById(crearEmpleadoRequest.getArea());
            
            if (!paisBuscado.isPresent() || !tipoIdentBuscado.isPresent() || !areaBuscada.isPresent()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("Error al buscar el país, el tipo de identificación y el área de empleo");
                return responseApi;
            }
            
            Empleado existeCedula = existeTipoIdentYNumIdent(tipoIdentBuscado.get().getCodTipoIdentificacion(), crearEmpleadoRequest.getNumeroIdentificacion());
            if (existeCedula != null) {
                responseApi.setbRta(false);
                responseApi.setsMsg("El tipo de identificación y número: " + tipoIdentBuscado.get().getNombre() + " - " + crearEmpleadoRequest.getNumeroIdentificacion() + " ya se encuentra registrado");
                return responseApi;
            }
            
            Empleado nuevoEmpleado = new Empleado();
            //Información buscada
            nuevoEmpleado.setCodArea(areaBuscada.get());
            nuevoEmpleado.setCodPais(paisBuscado.get());
            nuevoEmpleado.setCodTipoIdentificacion(tipoIdentBuscado.get());
            //Información por defecto
            nuevoEmpleado.setEstado("ACT");
            nuevoEmpleado.setFechaRegistro(new Date());
            //Información por 
            nuevoEmpleado.setPrimerApellido(crearEmpleadoRequest.getPrimerApellido());
            nuevoEmpleado.setSegundoApellido(crearEmpleadoRequest.getSegundoApellido());
            nuevoEmpleado.setPrimerNombre(crearEmpleadoRequest.getPrimerNombre());
            nuevoEmpleado.setOtrosNombres(crearEmpleadoRequest.getOtrosNombres());
            nuevoEmpleado.setNumeroIdentificacion(crearEmpleadoRequest.getNumeroIdentificacion());
            nuevoEmpleado.setPrimerApellido(crearEmpleadoRequest.getPrimerApellido());
            nuevoEmpleado.setFechaIngreso(convertirFecha(crearEmpleadoRequest.getFechaIngreso()));
            
            Empleado crearEmpleado = empleadoRepository.save(nuevoEmpleado);
            //Generación de correo
            String correoInicial = generarCorreo(crearEmpleadoRequest.getPrimerNombre(), crearEmpleadoRequest.getPrimerApellido(), "", paisBuscado.get());
            boolean existeCorreo = existeCorreo(correoInicial);
            if (existeCorreo) {
                correoInicial = generarCorreo(crearEmpleadoRequest.getPrimerNombre(), crearEmpleadoRequest.getPrimerApellido(), nuevoEmpleado.getCodEmpleado() + "", paisBuscado.get());
            }
            crearEmpleado.setCorreoElectronico(correoInicial);
            crearEmpleado = empleadoRepository.save(crearEmpleado);
            
            List<Empleado> listaEmpleado = new ArrayList<>();
            listaEmpleado.add(crearEmpleado);
            responseApi.setbRta(true);
            responseApi.setsMsg("Empleado creado correctamente");
            responseApi.setLista(listaEmpleado);
            return responseApi;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error en empleado controller => " + e.getMessage());
            responseApi.setbRta(false);
            responseApi.setsMsg("Ha ocurrido un error interno");
            return responseApi;
        }
    }
    
    public ResponseApi<Empleado> editarEmpleado(String id, CrearEmpleadoRequest crearEmpleadoRequest) {
        ResponseApi responseApi = new ResponseApi();
        try {
            
            responseApi = validarCampos(crearEmpleadoRequest);
            if (!responseApi.isbRta()) {
                return responseApi;
            }
            
            Optional<Pais> paisBuscado = paisRepository.findById(crearEmpleadoRequest.getPaisEmpleo());
            Optional<TipoIdentificacion> tipoIdentBuscado = tipoIdentificacionRepository.findById(crearEmpleadoRequest.getTipoIdentificacion());
            Optional<AreaEmpleado> areaBuscada = areaEmpleadoRepository.findById(crearEmpleadoRequest.getArea());
            
            if (!paisBuscado.isPresent() || !tipoIdentBuscado.isPresent() || !areaBuscada.isPresent()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("Error al buscar el país, el tipo de identificación y el área de empleo");
                return responseApi;
            }
            
            Optional<Empleado> empleadoBuscado = empleadoRepository.findById(Integer.parseInt(id));
            
            if (!empleadoBuscado.isPresent()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("No se ha encontrado un empleado");
                return responseApi;
            }
            
            Empleado existeCedula = existeTipoIdentYNumIdent(tipoIdentBuscado.get().getCodTipoIdentificacion(), crearEmpleadoRequest.getNumeroIdentificacion());
            if (existeCedula != null && existeCedula.getCodEmpleado() != empleadoBuscado.get().getCodEmpleado()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("El tipo de identificación y número: " + tipoIdentBuscado.get().getNombre() + " - " + crearEmpleadoRequest.getNumeroIdentificacion() + " ya se encuentra registrado");
                return responseApi;
            }

            //Información buscada
            empleadoBuscado.get().setCodArea(areaBuscada.get());
            empleadoBuscado.get().setCodPais(paisBuscado.get());
            empleadoBuscado.get().setCodTipoIdentificacion(tipoIdentBuscado.get());
            //Información por Digitación
            empleadoBuscado.get().setEstado(crearEmpleadoRequest.getEstado());
            empleadoBuscado.get().setPrimerApellido(crearEmpleadoRequest.getPrimerApellido());
            empleadoBuscado.get().setSegundoApellido(crearEmpleadoRequest.getSegundoApellido());
            empleadoBuscado.get().setPrimerNombre(crearEmpleadoRequest.getPrimerNombre());
            empleadoBuscado.get().setOtrosNombres(crearEmpleadoRequest.getOtrosNombres());
            empleadoBuscado.get().setNumeroIdentificacion(crearEmpleadoRequest.getNumeroIdentificacion());
            empleadoBuscado.get().setPrimerApellido(crearEmpleadoRequest.getPrimerApellido());
            String correoInicial = generarCorreo(crearEmpleadoRequest.getPrimerNombre(), crearEmpleadoRequest.getPrimerApellido(), "", paisBuscado.get());
            boolean existeCorreo = existeCorreo(correoInicial);
            if (existeCorreo) {
                correoInicial = generarCorreo(crearEmpleadoRequest.getPrimerNombre(), crearEmpleadoRequest.getPrimerApellido(), empleadoBuscado.get().getCodEmpleado() + "", paisBuscado.get());
            }
            empleadoBuscado.get().setCorreoElectronico(correoInicial);
            empleadoBuscado.get().setFechaModificacion(new Date());
            Empleado empleadoGuardado = empleadoRepository.save(empleadoBuscado.get());
            
            List<Empleado> listaEmpleado = new ArrayList<>();
            listaEmpleado.add(empleadoGuardado);
            responseApi.setbRta(true);
            responseApi.setsMsg("Empleado actualizado correctamente");
            responseApi.setLista(listaEmpleado);
            return responseApi;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error en empleado controller => " + e.getMessage());
            responseApi.setbRta(false);
            responseApi.setsMsg("Ha ocurrido un error interno");
            return responseApi;
        }
    }
    
    public ResponseApi<Empleado> eliminarEmpleado(String id) {
        ResponseApi responseApi = new ResponseApi();
        try {
            Optional<Empleado> buscarEmpleado = empleadoRepository.findById(Integer.parseInt(id));
            if (!buscarEmpleado.isPresent()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("No existe un empleado con el id buscado");
                return responseApi;
            }
            
            empleadoRepository.deleteById(Integer.parseInt(id));
            responseApi.setbRta(true);
            responseApi.setsMsg("Se ha eliminado el empleado de manera correcta");
            return responseApi;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error en empleado controller => " + e.getMessage());
            responseApi.setbRta(false);
            responseApi.setsMsg("Ha ocurrido un error interno");
            return responseApi;
        }
    }
    
    public ResponseApi<Empleado> buscarEmpleados(Pageable pageable) {
        ResponseApi<Empleado> responseApi = new ResponseApi<Empleado>();
        try {
            
            Page<Empleado> listaEmp = empleadoRepository.findAll(pageable);
            responseApi.setbRta(true);
            responseApi.setsMsg("Lista encontrada");
            responseApi.setListaPaginada(listaEmp);
            return responseApi;
        } catch (Exception e) {
            e.printStackTrace();
            return responseApi;
        }
    }
    
    public ResponseApi<Empleado> buscarEmpleadoPorId(String id) {
        ResponseApi<Empleado> responseApi = new ResponseApi<Empleado>();
        try {
            
            if (id == null || id.equals("undefined")) {
                responseApi.setbRta(false);
                responseApi.setsMsg("El código del empleado no es válido");
                return responseApi;
            }
            
            Optional<Empleado> listaEmp = empleadoRepository.findById(Integer.parseInt(id));
            if (!listaEmp.isPresent()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("No se ha encontrado un empleado");
                return responseApi;
            }
            
            List<Empleado> listaEmpleado = new ArrayList<>();
            listaEmpleado.add(listaEmp.get());
            responseApi.setbRta(true);
            responseApi.setsMsg("Lista encontrada");
            responseApi.setLista(listaEmpleado);
            return responseApi;
        } catch (Exception e) {
            responseApi.setbRta(false);
            responseApi.setsMsg("Ha ocurrido un error al buscar el empleado");
            e.printStackTrace();
            return responseApi;
        }
    }
    
    public ResponseApi<Empleado> validarCampos(CrearEmpleadoRequest crearEmpleadoRequest) {
        ResponseApi responseApi = new ResponseApi();
        try {
            
            Pattern patronAZ = Pattern.compile("^[A-Z]*");
            Pattern patronAZ2 = Pattern.compile("^[A-Z ]*");
            Pattern patronAZ3 = Pattern.compile("^[a-zA-Z0-9]*");
            
            Matcher primerApellidoPatron = patronAZ.matcher(crearEmpleadoRequest.getPrimerApellido());
            if (crearEmpleadoRequest.getPrimerApellido() == null && crearEmpleadoRequest.getPrimerApellido().length() > 20 || !primerApellidoPatron.matches()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("El primer apellido no debe tener más de 20 caracteres y no debe contener caracteres especiales, ni espacios ni Ñ. Debe ser en mayuscula");
                return responseApi;
            }
            
            Matcher segundoApellidoPatron = patronAZ.matcher(crearEmpleadoRequest.getSegundoApellido());
            if (crearEmpleadoRequest.getSegundoApellido() == null && crearEmpleadoRequest.getSegundoApellido().length() > 20 || !segundoApellidoPatron.matches()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("El segundo apellido no debe tener más de 20 caracteres y no debe contener caracteres especiales, ni espacios ni Ñ. Debe ser en mayuscula");
                return responseApi;
            }
            
            Matcher primerNombrePatron = patronAZ2.matcher(crearEmpleadoRequest.getPrimerNombre());
            if (crearEmpleadoRequest.getPrimerNombre() == null || crearEmpleadoRequest.getPrimerNombre().length() > 50 || !primerNombrePatron.matches()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("El primer nombre no debe tener más de 20 caracteres y no debe contener caracteres especiales, ni espacios ni Ñ. Debe ser en mayuscula");
                return responseApi;
            }
            
            Matcher otrosNombresPatron = patronAZ2.matcher(crearEmpleadoRequest.getOtrosNombres());
            if (crearEmpleadoRequest.getOtrosNombres() == null || crearEmpleadoRequest.getOtrosNombres().length() > 50 || !otrosNombresPatron.matches()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("Otros nombres no debe tener más de 50 caracteres y no debe contener caracteres especiales ni Ñ. Debe ser en mayuscula");
                return responseApi;
            }
            
            Matcher numeroIdentificacionPatron = patronAZ3.matcher(crearEmpleadoRequest.getNumeroIdentificacion());
            if (crearEmpleadoRequest.getNumeroIdentificacion() == null || crearEmpleadoRequest.getNumeroIdentificacion().length() > 20 || !numeroIdentificacionPatron.matches()) {
                responseApi.setbRta(false);
                responseApi.setsMsg("El número de identificación no debe tener más de 20 caracteres y no debe contener caracteres especiales, ni espacios ni Ñ.");
                return responseApi;
            }
            
            responseApi.setbRta(true);
            responseApi.setsMsg("Empleado creado correctamente");
            return responseApi;
        } catch (Exception e) {
            System.err.println("Error en empleado controller => " + e.getMessage());
            responseApi.setbRta(false);
            responseApi.setsMsg("Ha ocurrido un error interno");
            return responseApi;
        }
    }
    
    public ResponseApi<Empleado> buscarEmpleadosPorFiltro(Map<String, String> filtrosABuscar) {
        ResponseApi<Empleado> responseApi = new ResponseApi<>();
        responseApi.setbRta(true);
        List<Empleado> lista = empleadoRepositoryCustom.buscarEmpleadosFiltro(filtrosABuscar);
        responseApi.setLista(lista);
        return responseApi;
    }
    
    public boolean existeCorreo(String correo) {
        try {
            Empleado empleado = empleadoRepository.findByCorreoElectronico(correo);
            return empleado != null;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public Empleado existeTipoIdentYNumIdent(int tipoIdent, String numIdent) {
        try {
            Empleado empleado = empleadoRepository.findByTipoIdentAndNumeroIdentificacion(tipoIdent, numIdent);
            return empleado;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public String generarCorreo(String nombre, String apellido, String id, Pais pais) {
        String dominio = "@cidenet.com.co";
        if (pais.getCodPais() == 2) {
            dominio = "@cidenet.com.us";
        }
        String correo = nombre.toLowerCase() + "." + apellido.toLowerCase();
        if (!"".equals(id)) {
            correo += "." + id;
        }
        correo += dominio;
        return correo;
    }
    
    public Date convertirFecha(String fecha) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
            return date;
        } catch (ParseException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
}
