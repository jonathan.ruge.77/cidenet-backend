/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ruge2
 */
@Entity
@Table(name = "area_empleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AreaEmpleado.findAll", query = "SELECT a FROM AreaEmpleado a"),
    @NamedQuery(name = "AreaEmpleado.findByCodArea", query = "SELECT a FROM AreaEmpleado a WHERE a.codArea = :codArea"),
    @NamedQuery(name = "AreaEmpleado.findByNombre", query = "SELECT a FROM AreaEmpleado a WHERE a.nombre = :nombre")})
public class AreaEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_area")
    private Integer codArea;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;

    public AreaEmpleado() {
    }

    public AreaEmpleado(Integer codArea) {
        this.codArea = codArea;
    }

    public AreaEmpleado(Integer codArea, String nombre) {
        this.codArea = codArea;
        this.nombre = nombre;
    }

    public Integer getCodArea() {
        return codArea;
    }

    public void setCodArea(Integer codArea) {
        this.codArea = codArea;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codArea != null ? codArea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AreaEmpleado)) {
            return false;
        }
        AreaEmpleado other = (AreaEmpleado) object;
        if ((this.codArea == null && other.codArea != null) || (this.codArea != null && !this.codArea.equals(other.codArea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cidenet.application.entities.AreaEmpleado[ codArea=" + codArea + " ]";
    }
    
}
