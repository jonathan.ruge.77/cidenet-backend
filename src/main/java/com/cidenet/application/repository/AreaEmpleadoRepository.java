/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.repository;
import com.cidenet.application.entities.AreaEmpleado;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author ruge2
 */
public interface AreaEmpleadoRepository extends JpaRepository<AreaEmpleado,Integer> {
    
}

