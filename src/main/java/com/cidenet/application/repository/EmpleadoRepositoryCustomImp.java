/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.repository;

import com.cidenet.application.entities.Empleado;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author ruge2
 */
@Component
public class EmpleadoRepositoryCustomImp implements EmpleadoRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Empleado> buscarEmpleadosFiltro(Map<String, String> filtrosABuscar) {
        String query = "SELECT * FROM empleado e ";

        Map<String, String> filtrosDefinidos = obtenerFiltros();
        if (!filtrosABuscar.isEmpty()) {
            query += " where ";
            for (Map.Entry<String, String> entry : filtrosABuscar.entrySet()) {
                String condicion = filtrosDefinidos.get(entry.getKey()).replaceAll(":" + entry.getKey(), entry.getValue());
                query += "AND " + condicion;
            }
            query = query.replaceFirst("AND", "");
        }

        List<Empleado> listaFiltrada = em.createNativeQuery(query, Empleado.class).getResultList();
        return listaFiltrada;
    }

    public Map<String, String> obtenerFiltros() {
        Map<String, String> filtros = new HashMap<String, String>() {
            {
                put("primerNombre", "e.primer_nombre like '%:primerNombre%' ");
                put("otrosNombres", "e.otros_nombres like '%:otrosNombres%' ");
                put("primerApellido", "e.primer_apellido like '%:primerApellido%' ");
                put("segundoApellido", "e.segundo_apellido like '%:segundoApellido%' ");
                put("codTipoIdentificacion", "e.cod_tipo_identificacion = :codTipoIdentificacion ");
                put("numeroIdentificacion", "e.numero_identificacion like '%:numeroIdentificacion%' ");
                put("codPais", "e.cod_pais = :codPais ");
                put("correoElectronico", "e.correo_electronico like '%:correoElectronico%' ");
                put("estado", "e.estado like '%:estado%' ");
            }
        };

        return filtros;
    }

}
