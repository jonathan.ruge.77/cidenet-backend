/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.repository;

import com.cidenet.application.entities.Empleado;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ruge2
 */
public interface EmpleadoRepository extends JpaRepository<Empleado, Integer> {

    public Empleado findByCorreoElectronico(String correoElectronico);

    @Query(value = "SELECT * FROM empleado e WHERE e.cod_tipo_identificacion = :tipoIdent and e.numero_identificacion=:numeroIdent", nativeQuery = true)
    public Empleado findByTipoIdentAndNumeroIdentificacion(@Param("tipoIdent") int tipoIdent, @Param("numeroIdent") String numeroIdent);
    
    @Query(value = "Select * from empleado e", nativeQuery = true)
    public Page<Empleado> findPage(Pageable pageable);
    
}
