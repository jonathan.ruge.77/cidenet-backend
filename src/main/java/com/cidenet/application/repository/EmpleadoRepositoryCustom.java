/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.repository;

import com.cidenet.application.entities.Empleado;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ruge2
 */
public interface EmpleadoRepositoryCustom {

    List<Empleado> buscarEmpleadosFiltro(Map<String, String> filtrosABuscar);
}
