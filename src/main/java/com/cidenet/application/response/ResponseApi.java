/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.response;

import java.util.List;
import org.springframework.data.domain.Page;

/**
 *
 * @author ruge2
 */
public class ResponseApi<T> {
    private boolean bRta;
    private String sMsg;
    private List<T> lista;
    private Page<T> listaPaginada;

    public boolean isbRta() {
        return bRta;
    }

    public void setbRta(boolean bRta) {
        this.bRta = bRta;
    }

    public String getsMsg() {
        return sMsg;
    }

    public void setsMsg(String sMsg) {
        this.sMsg = sMsg;
    }

    public List<T> getLista() {
        return lista;
    }

    public void setLista(List<T> lista) {
        this.lista = lista;
    }

    public Page<T> getListaPaginada() {
        return listaPaginada;
    }

    public void setListaPaginada(Page<T> listaPaginada) {
        this.listaPaginada = listaPaginada;
    }
}
