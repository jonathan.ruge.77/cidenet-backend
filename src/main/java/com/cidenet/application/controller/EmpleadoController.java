/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.controller;
import com.cidenet.application.entities.Empleado;
import com.cidenet.application.request.CrearEmpleadoRequest;
import com.cidenet.application.response.ResponseApi;
import com.cidenet.application.service.EmpleadoService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ruge2
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "/v1/empleado")
public class EmpleadoController {

    @Autowired
    private EmpleadoService empleadoService;

    @RequestMapping(value = "/crearEmpleado", method = RequestMethod.POST)
    public ResponseApi<Empleado> crearEmpleado(@RequestBody CrearEmpleadoRequest crearEmpleadoRequest) {
        return empleadoService.crearEmpleado(crearEmpleadoRequest);
    }

    @RequestMapping(value = "/editarEmpleado/{id}", method = RequestMethod.PUT)
    public ResponseApi<Empleado> editarEmpleado(@PathVariable("id") String id, @RequestBody CrearEmpleadoRequest crearEmpleadoRequest) {
        return empleadoService.editarEmpleado(id, crearEmpleadoRequest);
    }

    @RequestMapping(value = "/eliminarEmpleado/{id}", method = RequestMethod.DELETE)
    public ResponseApi<Empleado> eliminarEmpleado(@PathVariable("id") String id) {
        return empleadoService.eliminarEmpleado(id);
    }

    @RequestMapping(value = "/buscarEmpleados", method = RequestMethod.GET)
    public ResponseApi<Empleado> buscarEmpleados(Pageable pageable) {
        return empleadoService.buscarEmpleados(pageable);
    }
    
    @RequestMapping(value = "/buscarEmpleadoPorId", method = RequestMethod.GET)
    public ResponseApi<Empleado> buscarEmpleadosPorId(@RequestParam("id") String id) {
        return empleadoService.buscarEmpleadoPorId(id);
    }
    
    @RequestMapping(value = "/buscarEmpleadosPorFiltro", method = RequestMethod.GET)
    public ResponseApi<Empleado> buscarEmpleados(@RequestParam Map<String,String> params) {
        return empleadoService.buscarEmpleadosPorFiltro(params);
    }

}
