/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.application.controller;

import com.cidenet.application.entities.AreaEmpleado;
import com.cidenet.application.entities.Pais;
import com.cidenet.application.entities.TipoIdentificacion;
import com.cidenet.application.response.ResponseApi;
import com.cidenet.application.service.SelectsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ruge2
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "/v1/selects")
public class SelectsController {
    
    @Autowired
    private SelectsService selectsService;
    
    @RequestMapping(value="/listaTipoIdentificacion",method = RequestMethod.GET)
    public ResponseApi<TipoIdentificacion> listaTipoIdentificacion(){
        return selectsService.listaTipoIdentificacion();
    }
    
    @RequestMapping(value="/listaAreaEmpleado",method = RequestMethod.GET)
    public ResponseApi<AreaEmpleado> listaAreaEmpleado(){
        return selectsService.listaAreaEmpleado();
    }
    
    @RequestMapping(value="/listaPaisEmpleo",method = RequestMethod.GET)
    public ResponseApi<Pais> listaPaisEmpleo(){
        return selectsService.listaPaisEmpleo();
    }
    
    
}
