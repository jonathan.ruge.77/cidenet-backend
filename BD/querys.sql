create database cidenet;
use cidenet;

create table pais(
	cod_pais integer not null auto_increment,
    nombre varchar(255) not null,
    primary key (cod_pais)
);

create table tipo_identificacion(
	cod_tipo_identificacion integer not null auto_increment,
    nombre varchar(255) not null,
    primary key (cod_tipo_identificacion)
);

create table area_empleado(
	cod_area integer not null auto_increment,
    nombre varchar(255) not null,
    primary key (cod_area)
);

create table empleado(
	cod_empleado integer not null auto_increment,
    primer_apellido varchar(20) not null,
    segundo_apellido varchar(20) not null,
    primer_nombre varchar(20) not null,
    otros_nombres varchar(50) null,
    cod_pais integer not null,
    cod_tipo_identificacion integer not null,
    numero_identificacion varchar(20) not null,
    correo_electronico varchar(300) null,
    fecha_ingreso datetime not null,
    cod_area integer not null,
    estado varchar(3) not null,
    fecha_registro datetime not null,
    fecha_modificacion datetime null,
    primary key (cod_empleado)
);

# Foreign key
ALTER TABLE empleado ADD FOREIGN KEY (cod_pais) REFERENCES pais(cod_pais);
ALTER TABLE empleado ADD FOREIGN KEY (cod_tipo_identificacion) REFERENCES tipo_identificacion(cod_tipo_identificacion);
ALTER TABLE empleado ADD FOREIGN KEY (cod_area) REFERENCES area_empleado(cod_area);

# Inserts default
INSERT INTO pais values (1,'Colombia'),
						(2,'Estados unidos');
                        
INSERT INTO tipo_identificacion VALUES (1,'Cédula de Ciudadanía'),
									   (2,'Cédula de Extranjería'),
									   (3,'Pasaporte'),
									   (4,'Permiso Especial');

INSERT INTO area_empleado values (1,'Administración'),
								 (2,'Financiera'),
								 (3,'Compras'),
                                 (4,'Infraestructura'),
                                 (5,'Operación'),
                                 (6,'Talento Humano'),
                                 (7,'Servicios Varios'),
                                 (8,'Otra');
                      