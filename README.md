# Cidenet
Proyecto creado con JAVA Spring boot. Es un proyecto para el backend de la prueba técnica de Cidenet.

## Correr el proyecto
El proyecto se creo con Netbeans, es decir, la manera de ejecutar con ese IDE es la siguiente
- Abrir Netbeans
- Archivo/Abrir proyecto
- Seleccionar el proyecto clonado y dar click en Abrir
- Esperar que carguen las dependencias
- Dar click derecho sobre el proyecto y despues en run o en debug
- El proyecto comenzará a compilar y finalmente se desplegará en la ruta: `http://localhost:8088/`
